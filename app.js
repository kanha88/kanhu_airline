//all js program of page 0


//active signup form
function opensignup(){
    let ele = document.getElementById('card1');
    let ele1 = document.getElementById('card2');
    let ele2 = document.getElementById('img1');

    if(ele.classList.contains('hide')){
        ele.classList.add('active');
        ele1.classList.remove('active');
        ele2.classList.add('hide');
    }

}

//active signin form
function opensignin(){
    let ele = document.getElementById('card1')
    let ele1 = document.getElementById('card2')
    let ele2 = document.getElementById('img1');

    if(ele1.classList.contains('hide')){
        ele1.classList.add('active');
        ele.classList.remove('active');
        ele2.classList.add('hide');
    }
}

//close the signp form
function close1(){
    let ele = document.getElementById('card1');
    let ele2 = document.getElementById('img1');

    if(ele.classList.contains('active')){
        ele.classList.remove('active');
        ele2.classList.remove('hide');
    }
}

//close the signin form
function close2(){
    let ele = document.getElementById('card1');
    let ele1 = document.getElementById('card2');
    let ele2 = document.getElementById('img1');

    if(ele1.classList.contains('active')){
        ele1.classList.remove('active');
        ele2.classList.remove('hide');
    }
}


//form validation of signup form
function validateform(){
    let ele = document.getElementById('enterfname');
    let ele1 = document.getElementById('enterlname');
    let ele2 = document.getElementById('enteremail');
    let ele3 = document.getElementById('enterpassword');
    let ele4 = document.getElementById('passworderror');
    let ele5 = document.getElementById('enterdob');

    if(ele.value == ""){
        ele.style.border = "2px solid red";
        return false;
    }else if(ele1.value == ""){
        ele1.style.border = "2px solid red";
        return false;
    }else if(ele2.value == ""){
        ele2.style.border = "2px solid red";
        return false;
    }else if(ele3.value == "" || ele3.value.length < 6 || ele3.value.length > 10){
        ele3.style.border = "2px solid red";
        ele4.style.color = "red";
        ele4.innerHTML = "Password should be btn 6 to 10 Character.";
        return false;
    }else if(ele4.value == ""){
        ele4.style.border = "2px solid red";
        return false;
    }else if(ele5.value == ""){
        ele5.style.border = "2px solid red";
        return false;
    }else{
        return true;
    }
}

function fname(){
    let ele =  document.getElementById('enterfname')

        if(ele.value !== ""){
            ele.style.border = "";
        }
}

function lname(){
    let ele = document.getElementById('enterlname')

        if(ele.value !== ""){
            ele.style.border = "";
        }
}

function email(){
    let ele = document.getElementById('enteremail')

        if(ele.value !== ""){
            ele.style.border = "";
        }
}

function pword(){
    let ele = document.getElementById('enterpassword');
    let ele1 = document.getElementById('passworderror');

        if(ele.value !== ""){
            ele.style.border = "";
            ele1.innerHTML = "";
        }
}


function dob(){
    let ele = document.getElementById('enterdob');

        if(ele.value !== ""){
            ele.style.border = "";
        }
}


//sign in form validation

function loginform(){
    let ele = document.getElementById('username');
    let ele1 = document.getElementById('userpass');
    let ele2 = document.getElementById('userpasswprderror')

    if(ele.value == ""){
        ele.style.border = "2px solid red";
        return false;
    }else if(ele1.value == "" || ele1.value.length < 6 || ele1.value.length > 10){
        ele1.style.border = "2px solid red";
        ele2.innerHTML = "Password should be in btn 6 to 10 character."
        ele2.style.color = "red";
        return false;
    }else{
        return true;
    };
};

function userlogin(){
    let lastname = document.getElementById("username");

    if(lastname.value !== ""){
        lastname.style.border = "";
    };
};

function passwordlogin(){
    let pass = document.getElementById('userpass');
    let info = document.getElementById('userpasswprderror')


    if(pass.value !== ""){
        pass.style.border == "";
        info.innerHTML = "";
        info.style.color = "";
    };
};


//select the tripp




// take the data from page 1 to page 2

const urlParameters = new URLSearchParams(window.location.search);

const firstname = urlParameters.get('enterfname');
const lastname = urlParameters.get('enterlname');
const enteremail = urlParameters.get('enteremail');
const password = urlParameters.get('enterpassword');
const dob = urlParameters.get('enterdob');

// const username = urlParameters.get('username');
// const userpass = urlParameters.get('userpass');


document.getElementById('profilename').innerText = firstname;
document.getElementById('fullname').innerText = "Full Name :- " + firstname + " " + lastname;
document.getElementById('emailid').innerText = "Email ID :- " + enteremail;
document.getElementById('password').innerText = "Password :- " + password;
document.getElementById('dob').innerText = "Date Of Birth:- " + dob;

// document.getElementById('profilename').innerText = username;
// document.getElementById('fullname').innerText = "Full Name :- " + username;
// document.getElementById('password').innerText = "Password :- " + userpass;